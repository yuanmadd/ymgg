# ymgg

十年团队资深全栈Java、Kotlin、C#、Vue、python、运维，测试。源码拍了立即发货，批量拿码有优惠，源码均可运行，其他问题也可询问。

所有源码非开源，2600多套源码添加vx【Live-Final】关注**gzh【源码哆哆】**进行咨询！

/**
     * 完成任务
     * @param taskVo 请求实体参数
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult complete(FlowTaskVo taskVo) {
        Task task = taskService.createTaskQuery().taskId(taskVo.getTaskId()).singleResult();
        if (Objects.isNull(task)) {
            return AjaxResult.error("任务不存在");
        }
        if (DelegationState.PENDING.equals(task.getDelegationState())) {
            taskService.addComment(taskVo.getTaskId(), taskVo.getInstanceId(), FlowComment.DELEGATE.getType(), taskVo.getComment());
            taskService.resolveTask(taskVo.getTaskId(), taskVo.getVariables());
        } else {
            taskService.addComment(taskVo.getTaskId(), taskVo.getInstanceId(), FlowComment.NORMAL.getType(), taskVo.getComment());
            Long userId = SecurityUtils.getLoginUser().getUser().getUserId();
            taskService.setAssignee(taskVo.getTaskId(), userId.toString());
            taskService.complete(taskVo.getTaskId(), taskVo.getVariables());
        }
        return AjaxResult.success();
    }

**【打开下方链接可获取源码、打开下方链接可获取源码、打开下方链接可获取源码】**
**打开运行必看文件，可以免费获得安装部署文档。**

**运行文件必看**

**lx作者获得完整源码：**https://wso3ld94dz.feishu.cn/docx/R8D9d384Qo4CN6xZpldcS1rCnff?from=from_copylink

**【打开上方链接可获取源码、打开上方链接可获取源码、打开上方链接可获取源码】**
